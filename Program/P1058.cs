using System;

class P1058 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a1 = Math.Log10((Math.PI + Math.PI) * n) * 0.5;
        var a2 = Math.Log10(n / Math.E) * n;
        Console.WriteLine((int)Math.Floor(a1 + a2) + 1);
    }
}
