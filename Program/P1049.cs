﻿using System;

class P1049 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        long ans = 0, c = 0;
        foreach (var i in a) {
            c = Math.Max(c, 0) + i;
            ans = Math.Max(ans, c);
        }
        Console.WriteLine(ans);
    }
}
