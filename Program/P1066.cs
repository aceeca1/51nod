﻿using System;

class P1066 {
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            var line = Console.ReadLine().Split();
            var n = int.Parse(line[0]);
            var k = int.Parse(line[1]);
            Console.WriteLine(n % (k + 1) != 0 ? "A" : "B");
        }
    }
}
