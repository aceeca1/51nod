﻿using System;
using System.Linq;

class P1000 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        Console.WriteLine(line.Sum(k => int.Parse(k)));
    }
}
