﻿using System;
using System.Numerics;

class P1012 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var a2 = int.Parse(line[1]);
        var gcd = BigInteger.GreatestCommonDivisor(a1, a2);
        Console.WriteLine(a1 / gcd * a2);
    }
}
