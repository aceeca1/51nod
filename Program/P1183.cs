using System;

class P1183 {
    public static void Main() {
        var a = Console.ReadLine();
        var b = Console.ReadLine();
        var dis = new int[a.Length + 1, b.Length + 1];
        for (int i = 0; i <= a.Length; ++i)
            for (int j = 0; j <= b.Length; ++j)
                if (i != 0 && j != 0 && a[i - 1] == b[j - 1])
                    dis[i, j] = dis[i - 1, j - 1];
                else if (i == 0 && j == 0) dis[i, j] = 0;
                else {
                    var d = int.MaxValue >> 1;
                    if (i != 0) {
                        var dNew = dis[i - 1, j] + 1;
                        if (dNew < d) d = dNew;
                    }
                    if (j != 0) {
                        var dNew = dis[i, j - 1] + 1;
                        if (dNew < d) d = dNew;
                    }
                    if (i != 0 && j != 0) {
                        var dNew = dis[i - 1, j - 1] + 1;
                        if (dNew < d) d = dNew;
                    }
                    dis[i, j] = d;
                }
        Console.WriteLine(dis[a.Length, b.Length]);
    }
}
