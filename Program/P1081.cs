using System;

class P1081 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new long[n + 1];
        for (int i = 1; i <= n; ++i) a[i] = long.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) a[i] += a[i - 1];
        var q = int.Parse(Console.ReadLine());
        for (int i = 0; i < q; ++i) {
            var line = Console.ReadLine().Split();
            var start = int.Parse(line[0]);
            var len = int.Parse(line[1]);
            Console.WriteLine(a[start + len - 1] - a[start - 1]);
        }
    }
}
