﻿using System;

class P1085 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var w = int.Parse(line[1]);
        var a = new int[w + 1];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var wi = int.Parse(line[0]);
            var pi = int.Parse(line[1]);
            for (int j = w; j >= wi; --j)
                a[j] = Math.Max(a[j], a[j - wi] + pi);
        }
        Console.WriteLine(a[w]);
    }
}
