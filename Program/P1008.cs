﻿using System;

class P1008 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var p = int.Parse(line[1]);
        long ans = 1;
        for (int i = 2; i <= n; ++i) ans = ans * i % p;
        Console.WriteLine(ans);
    }
}
