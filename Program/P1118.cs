using System;
using System.Numerics;

class P1118 {
    const int M = 1000000007;

    static int Inverse(int n) {
        return (int)BigInteger.ModPow(n, M - 2, M);
    }

    static int Choose(int n, int k) {
        long ans1 = 1, ans2 = 1;
        for (int i = 1; i <= k; ++i) {
            ans1 = ans1 * (n + 1 - i) % M;
            ans2 = ans2 * i % M;
        }
        long ans = ans1 * Inverse((int)ans2) % M;
        return (int)ans;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]) - 1;
        var m = int.Parse(line[1]) - 1;
        Console.WriteLine(Choose(n + m, n));
    }
}
