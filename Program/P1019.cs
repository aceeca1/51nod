﻿using System;
using System.Linq;

class P1019 {
    class SegTree {
        int M;
        int[] A;

        static int Ilogb(int n) {
            int ans = 0;
            while (n != 1) { n >>= 1; ++ans; }
            return ans;
        }

        public SegTree(int n) {
            M = 1 << (Ilogb(n + 2) + 1);
            A = new int[M + M];
        }

        public int Add(int u) {
            u += M;
            int ans = 0;
            for (; u > 1; u >>= 1) {
                if ((u & 1) == 0) ans += A[u + 1];
                ++A[u];
            }
            return ans;
        }
    }

    static int BSearch(int s, int t, Func<int, bool> func) {
        while (true) {
            if (s == t) return s;
            int m = s + ((t - s) >> 1);
            if (func(m)) t = m; else s = m + 1;
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        var b = a.OrderBy(k => k).Distinct().ToArray();
        for (int i = 0; i < n; ++i)
            a[i] = BSearch(0, b.Length, k => a[i] <= b[k]);
        int ans = 0;
        var tree = new SegTree(b.Length);
        foreach (var i in a) ans += tree.Add(i);
        Console.WriteLine(ans);
    }
}
