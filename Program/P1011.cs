﻿using System;
using System.Linq;
using System.Numerics;

class P1011 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var a2 = int.Parse(line[1]);
        Console.WriteLine(BigInteger.GreatestCommonDivisor(a1, a2));
    }
}
