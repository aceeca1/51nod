using System;

class P1174 {
    class SegTree {
        public int M;
        public int[] A;

        static int Ilogb(int n) {
            int ans = 0;
            while (n != 1) { n >>= 1; ++ans; }
            return ans;
        }

        public SegTree(int n) {
            M = 1 << (Ilogb(n + 2) + 1);
            A = new int[M + M];
        }

        public int RangeMax(int s, int t) {
            s += M - 1;
            t += M + 1;
            int ans = 0;
            while ((s ^ t) != 1) {
                if ((s & 1) == 0) ans = Math.Max(ans, A[s + 1]);
                if ((t & 1) != 0) ans = Math.Max(ans, A[t - 1]);
                s >>= 1;
                t >>= 1;
            }
            return ans;
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var st = new SegTree(n);
        for (int i = 1; i <= n; ++i)
            st.A[st.M + i] = int.Parse(Console.ReadLine());
        for (int i = st.M - 1; i != 0; --i)
            st.A[i] = Math.Max(st.A[i + i], st.A[i + i + 1]);
        var q = int.Parse(Console.ReadLine());
        for (int i = 0; i < q; ++i) {
            var line = Console.ReadLine().Split();
            var s = int.Parse(line[0]) + 1;
            var t = int.Parse(line[1]) + 1;
            Console.WriteLine(st.RangeMax(s, t));
        }
    }
}
