﻿using System;
using System.Numerics;

class P1057 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = BigInteger.One;
        for (int i = 2; i <= n; ++i) ans *= i;
        Console.WriteLine(ans);
    }
}
