using System;
using System.Linq;

class P1137 {
    static int[,] ReadMatrix(int n) {
        var ans = new int[n, n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            for (int j = 0; j < n; ++j) ans[i, j] = int.Parse(line[j]);
        }
        return ans;
    }

    static int[,] MulMatrix(int[,] a1, int[,] a2) {
        var n = a1.GetLength(0);
        var ans = new int[n, n];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                for (int k = 0; k < n; ++k)
                    ans[i, k] += a1[i, j] * a2[j, k];
        return ans;
    }

    static void WriteMatrix(int[,] a) {
        var n = a.GetLength(0);
        for (int i = 0; i < n; ++i) {
            var e = Enumerable.Range(0, n).Select(k => a[i, k]);
            Console.WriteLine(string.Join(" ", e));
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a1 = ReadMatrix(n);
        var a2 = ReadMatrix(n);
        WriteMatrix(MulMatrix(a1, a2));
    }
}
