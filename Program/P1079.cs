using System;
using System.Numerics;

class P1079 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var p = new int[n];
        var m = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            p[i] = int.Parse(line[0]);
            m[i] = int.Parse(line[1]);
        }
        long product = 1;
        for (int i = 0; i < n; ++i) product *= p[i];
        long sum = 0;
        for (int i = 0; i < n; ++i) {
            var pp = product / p[i];
            var ppInv = (long)BigInteger.ModPow(pp, p[i] - 2, p[i]);
            sum += m[i] * pp * ppInv;
        }
        Console.WriteLine(sum % product);
    }
}
