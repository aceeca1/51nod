﻿using System;

class P1106 {
    static bool IsPrime(int a) {
        for (int i = 2; i * i <= a; ++i) {
            if (a % i == 0) return false;
        }
        return true;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine(IsPrime(a) ? "Yes" : "No");
        }
    }
}
