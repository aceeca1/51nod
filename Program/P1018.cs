﻿using System;

class P1018 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        Array.Sort(a);
        foreach (var i in a) Console.WriteLine(i);
    }
}
