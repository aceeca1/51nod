﻿using System;

class P1006 {
    static string S1, S2;
    static int[,] A;
    
    static int ComputeA(ref int k1, ref int k2, out char c) {
        c = default(char);
        if (k1 == 0 || k2 == 0) return 0;
        if (S1[k1 - 1] == S2[k2 - 1]) {
            c = S1[k1 - 1];
            return A[--k1, --k2] + 1;
        }
        return A[k1 - 1, k2] > A[k1, k2 - 1] ? A[--k1, k2] : A[k1, --k2];
    }

    static int ComputeA(int k1, int k2) {
        char c;
        return ComputeA(ref k1, ref k2, out c);
    }

    public static void Main() {
        S1 = Console.ReadLine();
        S2 = Console.ReadLine();
        A = new int[S1.Length + 1, S2.Length + 1];
        for (int i1 = 0; i1 <= S1.Length; ++i1)
            for (int i2 = 0; i2 <= S2.Length; ++i2)
                A[i1, i2] = ComputeA(i1, i2);
        int k1 = S1.Length, k2 = S2.Length;
        var ans = new char[A[k1, k2]];
        for (;;) {
            char c;
            var a = ComputeA(ref k1, ref k2, out c);
            if (a == 0) break;
            if (c != default(char)) ans[a - 1] = c;
        }
        Console.WriteLine(new string(ans));
    }
}
