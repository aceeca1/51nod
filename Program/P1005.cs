﻿using System;
using System.Numerics;

class P1005 {
    public static void Main() {
        var a1 = BigInteger.Parse(Console.ReadLine());
        var a2 = BigInteger.Parse(Console.ReadLine());
        Console.WriteLine(a1 + a2);
    }
}
