using System;

class P1088 {
    static int MaxExtent(string s, int p1, int p2) {
        while (0 <= p1 && p2 < s.Length && s[p1] == s[p2]) {
            --p1;
            ++p2;
        }
        return p2 - p1 - 1;
    }

    public static void Main() {
        var s = Console.ReadLine();
        int answer = 0;
        for (int i = 0; i < s.Length; ++i) {
            var e1 = MaxExtent(s, i, i);
            if (answer < e1) answer = e1;
            var e2 = MaxExtent(s, i, i + 1);
            if (answer < e2) answer = e2;
        }
        Console.WriteLine(answer);
    }
}
