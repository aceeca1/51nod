﻿using System;

class P1134 {
    static int BSearch(int s, int t, Func<int, bool> func) {
        while (true) {
            if (s == t) return s;
            int m = s + ((t - s) >> 1);
            if (func(m)) t = m; else s = m + 1;
        }
    }

    static int LongestIncSubseq(int[] a) {
        var b = new int[a.Length - 1];
        for (int i = 0; i < b.Length; ++i) b[i] = int.MaxValue;
        b[0] = int.MinValue;
        foreach (var i in a)
            b[BSearch(0, b.Length, kk => i < b[kk])] = i;
        return BSearch(0, b.Length, k => b[k] == int.MaxValue) - 1;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        Console.WriteLine(LongestIncSubseq(a));
    }
}
